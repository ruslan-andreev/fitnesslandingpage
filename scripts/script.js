import smoothScroll from "./libjs/smoothScroll.js"
import headerScroll from "./libjs/headerScroll.js"
import accordion from "./libjs/accordion.js"
import validateEmail from "./libjs/validateEmail.js"
import scrollBtn from './libjs/scrollBtn.js' 
import animationHeroTitle from './libjs/animationHeroTitle.js'
import slider from './libjs/slider.js'
import mobileNavToggle from './libjs/mobileNavToggle.js'

import animationSVG from './libjs/animationSVG.js'
animationSVG()

smoothScroll()
headerScroll()
accordion()
slider()
validateEmail()
scrollBtn()
animationHeroTitle()
mobileNavToggle()


