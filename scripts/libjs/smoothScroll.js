function scrollHeader(){
    const links = document.querySelectorAll('.nav__link')
    const HEADER_OFFSET = 77

    links.forEach(element => {
        element.addEventListener('click', function(event){
            event.preventDefault()
        
            const sectionId = event.target.getAttribute('href').substr(1)

            const scrollToSection = document.getElementById(sectionId).offsetTop - HEADER_OFFSET
        
            window.scrollTo({
                top: scrollToSection,
                behavior: "smooth"
            })   
        })   
    })
}
export default scrollHeader
