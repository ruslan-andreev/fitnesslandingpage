const popUp = document.getElementById('popup')
const logInForm = document.getElementById('login')
const signUpForm = document.getElementById('signup')

const form = document.querySelectorAll('.form-popup')
form.forEach((element)=>{
   element.addEventListener('click', event=> event.stopPropagation())
})

const logInBtn = document.getElementById('popup-signin')
const signUpBtn = document.getElementById('popup-signup')
const signUpBtnWork = document.getElementById('popup-signup-work-section')

logInBtn.addEventListener('click', ()=>{
    logInForm.classList.toggle('form-popup--active')
    togglePopUp()
})
signUpBtn.addEventListener('click', ()=>{
    signUpForm.classList.toggle('form-popup--active')
    togglePopUp()
})
signUpBtnWork.addEventListener('click', ()=>{
    signUpForm.classList.toggle('form-popup--active')
    togglePopUp()
})

popUp.addEventListener('click', (event)=>{
    togglePopUp()
    signUpForm.classList.remove('form-popup--active')
    logInForm.classList.remove('form-popup--active')
})

const signUpLink = document.getElementById('link-signup')
const signInLink = document.getElementById('link-signin')
signInLink.addEventListener('click', changeForm)
signUpLink.addEventListener('click', changeForm)

const popUpBtnClose = document.querySelectorAll('.popup-btn-close')
popUpBtnClose.forEach(element => 
    element.addEventListener('click', ()=>{
        togglePopUp()
        signUpForm.classList.remove('form-popup--active')
        logInForm.classList.remove('form-popup--active')
    })
)

function changeForm() {
    logInForm.classList.toggle('form-popup--active')
    signUpForm.classList.toggle('form-popup--active')
}
function togglePopUp(){
    popUp.classList.toggle('popup--active')
}
