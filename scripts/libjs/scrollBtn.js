function scrollBtn(){
    let upBtn = document.getElementById('button-up')
    upBtn.addEventListener('click', scrollUp)
    
    window.addEventListener('scroll', () => {
        if(window.scrollY > 950){ 
            upBtn.classList.add('button--up--visible') 
        }else if(window.scrollY < 950){
            upBtn.classList.remove('button--up--visible')   
        }
    })
    
    function scrollUp (){
        window.scrollTo({
            top: 0,
            behavior: "smooth"
        }) 
    }
    
}
export default scrollBtn

