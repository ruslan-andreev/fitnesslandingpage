function mobileNavToggle(){
    const navToggle = document.querySelector('.nav-icon')

    if(navToggle){
        const navBody = document.querySelector('.header__nav')
        const navLink = document.querySelectorAll('.nav__link')

        navToggle.addEventListener('click', ()=>{
            navToggle.classList.toggle('nav-icon--active')
            toogleNav(navBody)
        })
        navLink.forEach(element =>{
            element.addEventListener('click', ()=>{
                navToggle.classList.toggle('nav-icon--active')
                toogleNav(navBody)
            })
        })
    }
    
    function  toogleNav(navBody){
        navBody.classList.toggle('header__nav--active')
    }
}
export default mobileNavToggle
