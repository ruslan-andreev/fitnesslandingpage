function slider(){
//ищим slider
    const slider = document.querySelector('.slider')
//создаем контейнер для кнопок
    const wrapperButtons = document.createElement('div')
    wrapperButtons.classList.add('slider-buttons')
// получаем элементы слайдера
    const sliderItems = document.querySelectorAll('.slider__item') || []
    const currentPosition = 0
    sliderItems[currentPosition].classList.add('slider__item--active')
//создаем будущий массив кнопок   
    const arrButtons = []
    
//создаем sliderBtn и пушим в массив кнопок, кнопки добавляем в HTML
    sliderItems.forEach((item, index) => {
        const sliderBtn = document.createElement('button')
        sliderBtn.classList.add('slider-button')
        sliderBtn.setAttribute('data-id', index)

        arrButtons.push(sliderBtn)
        arrButtons[currentPosition].classList.add('slider-button--active')

        wrapperButtons.appendChild(sliderBtn)
        slider.appendChild(wrapperButtons)
    })
//вешаем кнопкам event 
    arrButtons.forEach(itemBtn => {
        itemBtn.addEventListener('click',(event)=> {
            sliderChange(event.target.getAttribute('data-id'))
        })
    })
//сперва всем элементам удаляем класс, а после 
//добавляем по индескам равным data-id   
    function sliderChange(index){
        sliderItems.forEach(itemSlide => itemSlide.classList.remove('slider__item--active'))
        sliderItems[index].classList.add('slider__item--active')

        arrButtons.forEach(itemBtn => itemBtn.classList.remove('slider-button--active'))
        arrButtons[index].classList.add('slider-button--active')
    } 
}
export default slider