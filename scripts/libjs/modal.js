const modalContact = document.querySelector('.modal-contact')
const modalContent = document.querySelector('.modal-contact__content')
const modalContactBtn = document.getElementById('modal-contact-btn')
const closeContactBtn = document.getElementById('close-contact-btn') 

modalContact.addEventListener('click', toggleContact)
modalContent.addEventListener('click', (event)=> event.stopPropagation())
modalContactBtn.addEventListener('click', toggleContact)
closeContactBtn.addEventListener('click', toggleContact)

function toggleContact(){
    modalContact.classList.toggle('modal-contact--active')
}