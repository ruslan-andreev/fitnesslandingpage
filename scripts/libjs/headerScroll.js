function headerScroll (){
    const header = document.querySelector('.header')
    const scrollChange = 1

    const classAdd = () =>{
        header.classList.add('header--line')
    }
    const classRemove = () =>{
        header.classList.remove('header--line')
    }

    window.addEventListener('scroll', () =>{
        //console.log(window.scrollY)
        if(window.scrollY > scrollChange){
            classAdd()
        }
        else if(window.scrollY == 0){
            classRemove()
        }
    })  
}
export default  headerScroll
