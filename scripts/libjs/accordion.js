function accordion (){

    /*const accordionBtnToggle = document.querySelectorAll('.accordion-btn-toggle')
   
    accordionBtnToggle.forEach(item => {
        item.addEventListener('click', (event) => accordionToggle(event))
    })
    
    function accordionToggle (event){
        event.target.classList.toggle('accordion-btn-toggle--active')
        event.target.parentElement.parentElement.classList.toggle('accordion-item--active')
        event.target.parentElement.nextElementSibling.classList.toggle('accordion-item__content--active')  
    }*/

    const accordionToogle = document.querySelectorAll('.accordion-item__header')

    accordionToogle.forEach( item => {
        item.addEventListener('click', (event) =>accordioItemToggle(event))
    })

    function accordioItemToggle(event){
        const accordionHeader = event.currentTarget
        const accordionItem = event.currentTarget.parentElement
        const accordinContent = event.currentTarget.nextElementSibling
        accordionHeader.classList.toggle('accordion-item__header--active')
        accordionItem.classList.toggle('accordion-item--active')
        accordinContent.classList.toggle('accordion-item__content--active')
    }   
}
export default accordion