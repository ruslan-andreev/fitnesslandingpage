function validateEmail(){
    
    let btnSubscibe = document.getElementById('btn-subscribe')
    let btnStarted = document.getElementById('btn-start')

    btnSubscibe.addEventListener('click',()=>{
        const ID = 'input-subscribe'
        show(ID)
    })

    btnStarted.addEventListener('click',()=>{
        const ID = 'input-start'
        show(ID)
    })
  
    function checkEmail(email){
        let regExp = /^([a-z0-9_-]+\.)*[a-z0-9_-]+@[a-z0-9_-]+(\.[a-z0-9_-]+)*\.[a-z]{2,6}$/;
        return regExp.test(email)
    }
    
    function show(ID){
        let email = document.getElementById(`${ID}`).value
        let formLabelStart = document.getElementById('form-label-start')
        let formLabelSub = document.getElementById('form-label-subscribe')

        if(`${ID}` === 'input-subscribe'){
            if(checkEmail(email)){
                formLabelSub.innerHTML = 'Done!'
                formLabelSub.classList.remove('form-label--invalid')
            }else{
                formLabelSub.innerHTML = 'Email is invalid!'
                formLabelSub.classList.add('form-label--invalid')
            }
        }else{
            if(checkEmail(email)){
                formLabelStart.innerHTML = 'Done!'
                formLabelStart.classList.remove('form-label--invalid')
            }else{
                formLabelStart.innerHTML = 'Email is invalid!'
                formLabelStart.classList.add('form-label--invalid')
            }
        }         
    }
}
export default validateEmail